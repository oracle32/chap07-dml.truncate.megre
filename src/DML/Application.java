package DML;

public class Application {
	public static void main(String[] args) {
		
		
		/* DML 공부 2022-02-01
		 
		 데이터 조작언어, 테이블에 값을 삽입하거나, 수정하거나, 삭제하거나, 조회 하는 언어이다.
		 SELECT	      INSERT			UPDATE   DELETE
		 
		 
		 --------------------------------------------------- 1. INSERT 사용방법
		 
		 ex)
		 INSERT
		   INTO 테이블명 VALUES(데이터,데이터,...);
		 
		 
		 INSERT
		   INTO EMPOYEE E
		 (
		   E.EMP_ID, E.EMP_NAME, E.EMP_NO, E.EMP_EMAIL
		 )  
		 VALUES
		 (
		   200, '홍길동', '123456-1234567', hog@ogiraffers.com
		 );
		   
		   
		 // VALUES 대신 서브쿼리 사용하기
		 
		 CREATE TABLE EMP_01(
		   EMP_ID NUMBER,
		   EMP_NAME VARCHAR2(30),
		 );							-- 테이블 생성
		 
		 INSERT
		   INTO EMP_01 E1
		 (
		   E1.EMP_ID
		 , E1.EMP_NAME  
		 )  	
		 (
		 SELECT E.EMP_ID
		      , E.EMP_NAME
		   FROM EMPLOYEE E;   
		 )
		 
		 
		 
		 --------------------------------------------------- 2. UPDATE 사용방법
		 UPDATE 테이블명
		    SET 컬럼명 = 바꿀값,
		  WHERE 컬럼명 = 바꿀값, ....
		 
		 UPDATE EMPLOYEE E
		    SET E.EMP_ID = '201'
		  WHERE E.EMP_ID = '999';  
		 
		 
		 -- UPDATE 안에 서브쿼리 사용하기
		 -- SET 컬럼명 = (서브쿼리)
		 
		 UDDATE EMP_SALARY ES
		    SET (ES.EMP_NO, ES.EMP_ID) = (SELECT E.EMP_NO
		                                       , E.EMP_ID 
		                                    FROM EMPLOYEE E
		                                   WHERE E.EMP_NAME = '호랑이'    
		                                 )
		  WHERE ES.EMP_NAME IN ('토끼','거북이','사자')                               
		 
		 
		 토끼,거북이,사자의 EMP_NO,EMP_ID를 호랑이와 똑같이 만들어 주었다.
		 
		 
		 
		 
		 --------------------------------------------------- 3. DELETE 사용방법
		 
		 DELETE
		   FROM 테이블명
		  WHERE 조건설정;
		  
		  
		 DELETE
		   FROM EMPLOYEE
		  WHERE EMP_ID = '201';
		 -- EMPLOYEE 테이블의 EMP_ID 201번을 지우겠다.
		    
		 ROLLBACK; 이전복구
		 
		  
		 --------------------------------------------------- 3-2 TRUNCATE
		 테이블의 전체 행을 삭제 할 시 사용한다.
		 DELETE보다 빠르지만 ROLLBACK을 통해 복구할수 없다.
		 
		 TRUNCATE TABLE EMP_SALARY;      -- EMP_SALARY 영구삭제
		  
		  
		  
		  
		 --------------------------------------------------- MERGE
		 
		 -- MERGE : 구조가 같은 두 개의 테이블을 하나로 합치는 기능을 한다.
		 --         테이블에서 지정하는 조건의 값이 존재하면 UPDATE
		 --         조건의 값이 없으면 INSERT된다.    
		 -- A라는 테이블을 A+ 라는 테이블로 리뉴얼했다, 이걸 통합해주는걸 의미한다.
		  
		  
		MERGE
		  INTO EMP_M01 M1
		 USING EMP_M02 M2
		    ON (M1.EMP_ID = M2.EMP_ID)
		  WHEN MATCHED THEN
		UPDATE
		   SET M1.EMP_NAME = M2.EMP_NAME
		     , M1.EMP_NO = M2.EMP_NO
		     , M1.EMAIL = M2.EMAIL
		     , M1.PHONE = M2.PHONE
		     , M1.DEPT_CODE = M2.DEPT_CODE
		     , M1.JOB_CODE = M2.JOB_CODE
		     , M1.SAL_LEVEL = M2.SAL_LEVEL
		     , M1.SALARY = M2.SALARY
		     , M1.BONUS = M2.BONUS
		     , M1.MANAGER_ID = M2.MANAGER_ID
		     , M1.HIRE_DATE = M2.HIRE_DATE
		     , M1.ENT_DATE = M2.ENT_DATE
		     , M1.ENT_YN = M2.ENT_YN
		  WHEN NOT MATCHED THEN
		  
		INSERT
		(
		  M1.EMP_ID, M1.EMP_NAME, M1.EMP_NO, M1.EMAIL, M1.PHONE
		, M1.DEPT_CODE, M1.JOB_CODE, M1.SAL_LEVEL, M1.SALARY, M1.BONUS
		, M1.MANAGER_ID, M1.HIRE_DATE, M1.ENT_DATE, M1.ENT_YN  
		)
		VALUES
		(
		  M2.EMP_ID, M2.EMP_NAME, M2.EMP_NO, M2.EMAIL, M2.PHONE
		, M2.DEPT_CODE, M2.JOB_CODE, M2.SAL_LEVEL, M2.SALARY, M2.BONUS
		, M2.MANAGER_ID, M2.HIRE_DATE, M2.ENT_DATE, M2.ENT_YN  
		);
		
		
		SELECT
		       EM.*
		  FROM EMP_M01 EM;  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		 
		 
		 
		 

		 */
		
		
	}

}
